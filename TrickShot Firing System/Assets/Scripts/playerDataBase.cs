﻿using UnityEngine;
using System.Collections;

public class playerDataBase : MonoBehaviour {

	//Variables to control play
	private bool 			paused = false;
	private string			selectedSpell;
	private string 			selectedEnchanment;


	public void SetPause(bool pause)
	{
		paused = pause;
	}
	public bool GetPause()
	{
		return paused;
	}
	public void SetSpell(string spell)
	{
		selectedSpell = spell;
	}
	public string GetSpell()
	{
		return selectedSpell;
	}
	public void SetEnchanment(string ench)
	{
		selectedEnchanment = ench;
	}
	public string GetEnchanment()
	{
		return selectedEnchanment;
	}
}
