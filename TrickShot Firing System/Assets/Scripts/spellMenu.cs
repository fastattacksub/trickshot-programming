﻿using UnityEngine;
using System.Collections;

public class spellMenu : MonoBehaviour {

	//public varaibles
	public GUISkin				spellSelectSkin;
	public string[] 			spellNames;
	public Texture2D[]			spellImagesSelected;
	public Texture2D[]			spellImagesNotSelected;
	public Texture2D            selectorBackground;
	public Vector2[]          	spellIconLocs;
	public Vector2            	iconSize;
	public bool					Spell;



	//private variables
	private int					selectedSpell;
	private string				selectedSpellName;
	private GameObject			player;
	private playerDataBase   	dataBaseScript;


	//initialize
	void Start()
	{
		player = GameObject.FindGameObjectWithTag("Player");
		dataBaseScript = player.GetComponent<playerDataBase>();
		selectedSpell = System.Array.IndexOf(spellNames, dataBaseScript.GetSpell());
		if(selectedSpell<0)
		{
			selectedSpell = 0;
		}
		SetSpell(selectedSpell);
	}

	void Update()
	{
		int numSpells = spellNames.Length;
	
		if (Input.GetAxis("Mouse ScrollWheel") > 0) 
		{
			selectedSpell++; 
			if(selectedSpell > (numSpells-1))
			{
				selectedSpell = selectedSpell - numSpells;
			}
			SetSpell(selectedSpell);
		}
		if (Input.GetAxis("Mouse ScrollWheel") < 0) 
		{
			selectedSpell--; 
			if(selectedSpell < (0))
			{
				selectedSpell = selectedSpell + numSpells;
			}
			SetSpell(selectedSpell);
		}
	}

	//Control GUI Display
	void OnGUI()
	{
		GUI.BeginGroup(ResizeGUI(new Rect(275,350, 600,400)));
		GUI.DrawTexture(ResizeGUI(new Rect(100, 0, 400,400)), selectorBackground, ScaleMode.ScaleToFit);
		for(int i=0;i < (spellNames.Length); i++)
		{
			if(selectedSpell == i)
			{
				GUI.DrawTexture(ResizeGUI(new Rect(spellIconLocs[i].x, spellIconLocs[i].y, iconSize.x,iconSize.y)), spellImagesSelected[i], ScaleMode.ScaleToFit);
			}
			else
			{
				GUI.DrawTexture(ResizeGUI(new Rect(spellIconLocs[i].x, spellIconLocs[i].y, iconSize.x,iconSize.y)), spellImagesNotSelected[i], ScaleMode.ScaleToFit);
			}
		}
		GUI.DrawTexture(ResizeGUI(new Rect(300-(iconSize.x/2), 200-(iconSize.y/2), iconSize.x,iconSize.y)), spellImagesSelected[selectedSpell], ScaleMode.ScaleToFit);
		GUI.EndGroup();
	}

	private void SetSpell(int spellselect)
	{
		selectedSpellName = spellNames[spellselect];
		if(Spell)
		{
			dataBaseScript.SetSpell(selectedSpellName);
		}
		else
		{
			dataBaseScript.SetEnchanment(selectedSpellName);
		}
	}
	private Rect ResizeGUI(Rect _rect)
	{
		float FilScreenWidth = _rect.width / 1228;
		float rectWidth = FilScreenWidth * Screen.width;
		float FilScreenHeight = _rect.height / 921;
		float rectHeight = FilScreenHeight * Screen.height;
		float rectX = (_rect.x / 1228) * Screen.width;
		float rectY = (_rect.y / 921) * Screen.height;
		
		return new Rect(rectX,rectY,rectWidth,rectHeight);
	}
}