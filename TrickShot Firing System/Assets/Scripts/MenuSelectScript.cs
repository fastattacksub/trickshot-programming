﻿using UnityEngine;
using System.Collections;

public class MenuSelectScript : MonoBehaviour {

	//public variables
	public GUIStyle  		labelStyle;
	public GameObject[]     prefabMenus;
	public string[]			menuNames;
	public Vector2 			objectLocation;
	public int				labelLeft;
	public int 				labelTop;
	public KeyCode			pauseKey;
	public KeyCode 			switchKey;
	public GameObject			player;

	//private variables
	private GameObject 			activeItem;
	private bool  				paused;
	private int					ringSelect;
	private bool				updateMenu;
	private playerDataBase   	dataBaseScript;

	void Start()
	{

		paused = false;
		ringSelect = 0;
		dataBaseScript = player.GetComponent<playerDataBase>();
	}

	void Update()
	{
		if(Input.GetKeyDown(pauseKey))
		{
			if(paused)
			{
				paused = false;
				dataBaseScript.SetPause(false);
				Destroy(activeItem);
			}
			else
			{
				paused = true;
				dataBaseScript.SetPause(true);
				activeItem = Instantiate(prefabMenus[0], objectLocation, Quaternion.identity) as GameObject;
				activeItem.transform.parent = transform;
				ringSelect = 0;
			}
		}
		if(paused)
		{
			if(Input.GetKeyDown(switchKey))
			{
				Debug.Log ("up");
				updateMenu = true;
			}
		}
		if(Input.GetKeyDown (KeyCode.Escape))
		{
			Application.Quit();
		}
	}


	void OnGUI()
	{
		if(paused == true)
		{
			GUI.BeginGroup(ResizeGUI(new Rect(50,150,600,150)));
			GUI.Label(ResizeGUI(new Rect(labelLeft,labelTop,200,50)),menuNames[ringSelect],labelStyle);

	
			if(updateMenu)
			{
				ringSelect = ringSelect + 1;
				if(ringSelect == prefabMenus.Length)
				{ ringSelect = 0;}
				Debug.Log(ringSelect);
				Destroy(activeItem);
				activeItem = Instantiate(prefabMenus[ringSelect], objectLocation, Quaternion.identity) as GameObject;
				activeItem.transform.parent = transform;
				updateMenu = false;
			}

			GUI.EndGroup();
		}
	}


	private Rect ResizeGUI(Rect _rect)
	{
		float FilScreenWidth = _rect.width /1024;
		float rectWidth = FilScreenWidth * Screen.width;
		float FilScreenHeight = _rect.height / 768;
		float rectHeight = FilScreenHeight * Screen.height;
		float rectX = (_rect.x / 1024) * Screen.width;
		float rectY = (_rect.y / 768) * Screen.height;
		
		return new Rect(rectX,rectY,rectWidth,rectHeight);
	}

	private int ResizeFont(int fSize)
	{
		int fsize = 0;
		int xsize = (int)((Screen.width/1024) * fsize); 
		int ysize =	(int)((Screen.height/768) * fsize);
		if(xsize < ysize)
		{
			fsize = xsize;
		}
		else
		{
			fsize = ysize;
		}

		return fsize;
	}
}
