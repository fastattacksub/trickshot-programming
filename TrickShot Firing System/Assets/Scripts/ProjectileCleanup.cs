﻿using UnityEngine;
using System.Collections;

public class ProjectileCleanup : MonoBehaviour {
	//private variables

	public float 				lifeTime;		

	private float				curTime;

	// Use this for initialization
	void Start () {
		curTime = 0;
	
	}
	
	// Update is called once per frame
	void Update () {
		curTime += Time.deltaTime;
		if(curTime > lifeTime)
		{
			Destroy(gameObject);
		}
	
	}
}
