﻿using UnityEngine;
using System.Collections;

public class SpellSystem : MonoBehaviour {

	//public variables
	public float			firingStrength;
	public float     		firingAngle;
	public float			maxFiringTime;
	public float 			defaultStrength;
	public float 			strengthFactor;
	public float    		angleFactor;
	public GameObject[]		firingObject;
	public float 			maxFiringAngle;
	public float 			maxFiringStrength;
	public float 			delayTime;
	public GameObject 		indicatorLight;
	public GameObject		player;
	public string[]			spellNames;
	public Color 			c1 = Color.red;
	public Color 			c2 = Color.blue;



	//private variables
	private float 				firingTime;
	private bool 				firing;
	private Vector2 			initPos;
	private float				trueAngle;
	private bool				delay;
	private float 				dTime; 
	private playerDataBase   	dataBaseScript;
	private int					spellNumber;
	private float 				lineFactor = 0.05f;
	private LineRenderer 		lineRenderer;

	// Use this for initialization
	void Start () 
	{
		firingTime = 0;
		firingAngle = 0;
		firingStrength = 0;
		firing = false;
		dTime = 0;
		indicatorLight.renderer.material.color = Color.green;
		dataBaseScript = player.GetComponent<playerDataBase>();
		spellNumber = System.Array.IndexOf(spellNames, dataBaseScript.GetSpell());
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(dataBaseScript.GetPause() == false)
		{
			if(!firing)
			{
				if(delay)
				{
					dTime += Time.deltaTime;
					if(dTime > delayTime)
					{
						delay = false;
						indicatorLight.renderer.material.color = Color.green;
					}
					else
					{
						indicatorLight.renderer.material.color = Color.red;
					}
				}
				if(Input.GetMouseButtonDown(0))
				{
					if(!delay)
					{
						firing = true;
						firingTime = 0;
						initPos = Input.mousePosition;
						lineRenderer = gameObject.AddComponent<LineRenderer>();
						lineRenderer.material = new Material(Shader.Find("Particles/Additive"));
						lineRenderer.SetColors(c1, c2);
						lineRenderer.SetVertexCount(2);
						lineRenderer.SetWidth(0.2F, 0.2F);
					}
				}
			}
			else
			{
				Vector2 curMP = Input.mousePosition;
				if((initPos.x - curMP.x) == 0)
				{
					trueAngle = 0;
				}
				else
				{
					trueAngle = CalculateAngle(initPos.y - curMP.y,initPos.x - curMP.x);
				}
				firingAngle = Mathf.Clamp(trueAngle * angleFactor,-maxFiringAngle,maxFiringAngle);
				firingStrength = Mathf.Clamp((initPos.x - curMP.x) * strengthFactor + defaultStrength,0,maxFiringStrength);
				float llength = firingStrength * lineFactor;
				UpdateLine(llength, firingAngle);

				firingTime += Time.deltaTime;
				if(firingTime > maxFiringTime)
				{
					FireSpell(defaultStrength, firingAngle);
					firing = false;
					firingTime = 0;
				}
				if(Input.GetMouseButtonUp(0))
				{
					FireSpell(firingStrength, firingAngle); 
					firing = false;
					firingTime = 0;
				}
			}	
		}
	}

	void FireSpell(float Strength, float Angle)
	{
		spellNumber = System.Array.IndexOf(spellNames, dataBaseScript.GetSpell());
		if(spellNumber <0)
		{
			spellNumber =0;
		}
		GameObject clone = Instantiate(firingObject[spellNumber], transform.position, transform.rotation) as GameObject; 
		Vector3 dir = Quaternion.AngleAxis(firingAngle, Vector3.forward) * Vector3.right;
		clone.rigidbody.AddForce(dir*firingStrength);
		dTime = 0;
		delay = true;
		Destroy(lineRenderer);
	}

	float CalculateAngle(float disone, float distwo)
	{
		float tanA = disone/distwo;
		return tanA * Mathf.Rad2Deg;
	}
	void UpdateLine(float length, float angle)
	{
		Vector3 gpos = gameObject.transform.position;
		float posy = 0;

		if(angle == 0)
		{
			posy = 0;
		}
		else
		{
			posy = length/(1/Mathf.Tan(Mathf.Deg2Rad*angle));
		}

		Debug.Log ("length =: " + length.ToString()+" angle =: " + angle.ToString()+ " posy =: "+ posy.ToString() + " firingStrength: " + firingStrength.ToString() );

		Vector3 pos = new Vector3(gpos.x, gpos.y, 0);
		lineRenderer.SetPosition(0, pos);
		pos = new Vector3(gpos.x + length, gpos.y + posy, 0);
		lineRenderer.SetPosition(1, pos);
	}
}
